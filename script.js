let JSON_DATA = '';
const JSON_REQUEST = new XMLHttpRequest;

JSON_REQUEST.open('GET', 'https://picsum.photos/v2/list?limit=10', true);
JSON_REQUEST.onload = function () {
  if (JSON_REQUEST.status >= 200 && JSON_REQUEST.status < 400) {
    JSON_DATA = JSON.parse(JSON_REQUEST.responseText);
    generateImages();
  } else {
    console.log('something went wrong...')
  }
};
JSON_REQUEST.send();

function generateActiveClass(index) {
  return index === 0 ? 'active' : ''
}

function generateImages() {
  $.each(JSON_DATA, function (index, obj) {
    $('.carousel-inner').append(
      `<div class="carousel-item ${generateActiveClass(index)}">
        <img class="d-block w-100" 
             src="${obj.download_url}" 
             alt="${obj.id}">
        <div class="author">
          ${obj.author}
        </div>
      </div>`
    )
  })
}

$(document).ready(function () {

  setInterval(function () {
    const hours = new Date().getHours();
    const minutes = new Date().getMinutes();
    const seconds = new Date().getSeconds();
    const days = new Date().getDate();
    const months = new Date().getMonth();
    const year = new Date().getFullYear();
    $('.hours').html((hours < 10 ? '0' : '') + hours);
    $('.min').html((minutes < 10 ? '0' : '') + minutes);
    $('.sec').html((seconds < 10 ? '0' : '') + seconds);
    $('.day').html(days);
    $('.month').html((months < 10 ? '0' : '') + months);
    $('.year').html(year);
  }, 1000);

  $('body').click(function (e) {
    if (e.target.tagName !== 'BUTTON') {
      $('button').animate({backgroundColor: '#1f7ccc', color: '#1f7ccc'}, 300);
      $('.text-extender').animate({backgroundColor: 'transparent', color: '#2699FB'}, 300);
      $('.open-gallery').animate({backgroundColor: '#2699FB', color: '#fff'}, 300);
      $('.close').animate({backgroundColor: '#F0F9FF', color: '#2699FB'}, 300);
    }
  });

});
